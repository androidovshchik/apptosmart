package com.apptosmart;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;

import com.apptosmart.database.TableMenu;
import com.apptosmart.items.ItemMenu;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ActivityMain extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = ActivityMain.class.getSimpleName();
    private static final String STATE_MENU_POSITION = "menu_position";

    private SlidingMenu slidingMenu;
    private List<ItemMenu> menu;
    private RecyclerView.Adapter adapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ViewTreeObserver.OnScrollChangedListener listener;
    int menuPosition;

    private SharedPreferences memory;

    private WebView web;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_MENU_POSITION, menuPosition);
    }

    @Override
    @SuppressLint("SetJavaScriptEnabled")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        Locale locale = new Locale("en");
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.setLocale(locale);
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
        setContentView(R.layout.activity_main);

        memory = getApplicationContext().getSharedPreferences(Constants.APP_TO_SMART, Context.MODE_PRIVATE);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        View bar = getLayoutInflater().inflate(R.layout.toolbar, (ViewGroup) findViewById(android.R.id.content), false);
        toolbar.addView(bar);
        if(getSupportActionBar() != null)
            getSupportActionBar().setDisplayShowTitleEnabled(false);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setNavigationBarColor(ContextCompat.getColor(getApplicationContext(), R.color.background_main));
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.background_main));
        }

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipeRefreshLayout.setOnRefreshListener(this);
        ProgressBar progress = (ProgressBar) findViewById(R.id.progress);

        web = (WebView) findViewById(R.id.web);
        web.getSettings().setJavaScriptEnabled(true);
        web.setWebViewClient(new WebClient(swipeRefreshLayout, progress));
        if(!isOnline()){
            showMessage();
        }

        listener = new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                swipeRefreshLayout.setEnabled(web.getScrollY() == 0);
            }
        };
        swipeRefreshLayout.getViewTreeObserver().addOnScrollChangedListener(listener);

        if(savedInstanceState != null) {
            menuPosition = savedInstanceState.getInt(STATE_MENU_POSITION);
        } else {
            menuPosition = 0;
        }

        if(!TableMenu.getInstance().isTableEmpty(Constants.TABLE_MENU)) {
            menu = TableMenu.getInstance().getAllMenu();
            menu.get(menuPosition).setActive(true);
            if(!menu.get(0).getLink().trim().isEmpty())
                web.loadUrl(menu.get(0).getLink());
        } else
            menu = new ArrayList<>();
        loadMenu();


        slidingMenu = new SlidingMenu(this);
        slidingMenu.setMode(SlidingMenu.RIGHT);
        slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
        slidingMenu.setFadeEnabled(false);
        slidingMenu.setBehindWidth(dp2px(270));
        slidingMenu.setBehindScrollScale(0.5f);
        slidingMenu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        slidingMenu.setMenu(R.layout.menu);
        settingMenu((RecyclerView) slidingMenu.getMenu());
    }

    private void settingMenu(RecyclerView recyclerView) {
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        recyclerView.setHasFixedSize(true);

        adapter = new RecyclerView.Adapter() {

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
                switch (position) {
                    case Constants.TYPE_IMAGE:
                        return new ViewHolder(new NetworkImageView(getApplicationContext()), position);
                    default:
                        // item
                        return new ViewHolder(new TextView(getApplicationContext()), position);
                }
            }

            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
                if(position != 0) {
                    final int finalPosition = position - 1;
                    if(menu.get(finalPosition).isActive()) {
                        holder.itemView.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.item_menu_dark_background));
                    } else {
                        holder.itemView.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.item_menu_background));
                    }
                    ((TextView) holder.itemView).setText(menu.get(finalPosition).getTitle());
                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            notifyMenu(finalPosition);
                            callMenu(null);
                            if(!isOnline())
                                showMessage();
                            if(!menu.get(finalPosition).getLink().isEmpty())
                                web.loadUrl(menu.get(finalPosition).getLink());
                        }
                    });
                } else {
                    String menuImage = memory.getString(Constants.MEMORY_IMG_MENU, Constants.EMPTY);
                    if(!menuImage.trim().isEmpty())
                        ((NetworkImageView) holder.itemView).setImageUrl(menuImage, AppToSmart.getInstance().getImageLoader());
                }
            }

            @Override
            public int getItemViewType(int position) {
                if(position == 0) {
                    return Constants.TYPE_IMAGE;
                } else {
                    return Constants.TYPE_ITEM;
                }
            }

            @Override
            public int getItemCount() {
                return menu.size() + 1;
            }

            class ViewHolder extends RecyclerView.ViewHolder {

                public ViewHolder(View itemView, int viewType) {
                    super(itemView);
                    if(viewType == Constants.TYPE_ITEM) {
                        itemView.setPadding(dp2px(16), dp2px(8), dp2px(16), dp2px(8));
                        itemView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, dp2px(40)));
                        ((TextView) itemView).setTextColor(Color.WHITE);
                        ((TextView) itemView).setTypeface(((TextView) itemView).getTypeface(), Typeface.BOLD);
                        ((TextView) itemView).setSingleLine(true);
                        ((TextView) itemView).setEllipsize(TextUtils.TruncateAt.END);
                    } else {
                        // header image
                        itemView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                        ((NetworkImageView) itemView).setScaleType(ImageView.ScaleType.CENTER_CROP);
                    }
                }
            }
        };
        recyclerView.setAdapter(adapter);
    }

    private void notifyMenu(int finalPosition) {
        menuPosition = finalPosition;
        int prevPosition = 0;
        for(int i = 0; i < menu.size(); i++) {
            if(menu.get(i).isActive()) {
                menu.get(i).setActive(false);
                prevPosition = i;
            }
        }
        menu.get(finalPosition).setActive(true);
        adapter.notifyItemChanged(prevPosition + 1);
        adapter.notifyItemChanged(finalPosition + 1);
    }

    public void callMenu(View view) {
        // called as from hamburger button as from code
        slidingMenu.toggle();
    }

    @Override
    public void onBackPressed() {
        if(slidingMenu.isMenuShowing()) {
            slidingMenu.toggle();
        } else if(web.canGoBack()) {
            web.goBack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(listener != null)
            swipeRefreshLayout.getViewTreeObserver().removeOnScrollChangedListener(listener);
        if(web != null) {
            web.destroy();
        }
        web = null;
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }

    private void loadMenu() {
        StringRequest request = new StringRequest(Constants.URL_GET_MENU, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... objects) {
                        String title, link;
                        int active;
                        try {
                            JSONObject obj = new JSONObject(response);
                            TableMenu.getInstance().deleteAllItems(Constants.TABLE_MENU);
                            menu.clear();
                            obj.getString(Constants.API);
                            String menuImage = obj.getString(Constants.IMG_MENU);
                            SharedPreferences.Editor editor = memory.edit();
                            editor.putString(Constants.MEMORY_IMG_MENU, menuImage);
                            editor.apply();
                            if(obj.has(Constants.ERROR))
                                return null;
                            JSONArray array = obj.getJSONArray(Constants.MENU);
                            for(int i = 0; i < array.length(); i++) {
                                JSONObject item = (JSONObject) array.get(i);
                                JSONObject objTitle = item.getJSONObject(Constants.MENU_TITLE_OBJECT);
                                title = objTitle.getString(Constants.EN_PARAM);
                                link = item.getString(Constants.MENU_LINK_PARAM);
                                active = item.getInt(Constants.MENU_ACTIVE_PARAM);
                                ItemMenu itemMenu = new ItemMenu(title, link);
                                TableMenu.getInstance().addItem(itemMenu);
                                if(active == Constants.VALUE_TO_BE_SHOWN_IN_ACTIVE_PARAM)
                                    menu.add(itemMenu);
                            }
                        } catch (JSONException e) {
                            Log.e(TAG, e.toString());
                        }
                        menu.get(menuPosition).setActive(true);
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void result) {
                        adapter.notifyDataSetChanged();
                        if(web != null)
                            web.loadUrl(menu.get(0).getLink());
                    }
                }.execute();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, error.toString());
            }
        });
        AppToSmart.getInstance().getRequestQueue().add(request);
    }

    @Override
    public void onRefresh() {
        web.reload();
    }

    private boolean isOnline() {
        final ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }

    private void showMessage() {
        Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinator), getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG);
        snackbar.show();
    }
}
