package com.apptosmart.items;

public class ItemMenu {

    private long id;
    private String title;
    private String link;
    private boolean isActive = false;

    public ItemMenu() {}

    public ItemMenu(String title, String link) {
        this.title = title;
        this.link = link;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setActive(boolean active) {
        this.isActive = active;
    }

    public boolean isActive() {
        return this.isActive;
    }

    public String getTitle() {
        return this.title;
    }

    public String getLink() {
        return this.link;
    }
}