package com.apptosmart;

public interface Constants {

    /* main parameters */
    String APP_TO_SMART = "AppToSmart";
    String EMPTY = "";
    String API = "API";
    String ERROR = "error";
    String EN_PARAM = "EN";
    //String IT_PARAM = "IT";

    /* View types */
    int TYPE_IMAGE = 0;
    int TYPE_ITEM = 1;

    /* Database parameters */
    int DATABASE_VERSION = 2;
    String DATABASE_NAME = APP_TO_SMART;
    String TABLE_MENU = "menu";
    String KEY_ID = "id";
    String KEY_TITLE = "title";
    String KEY_LINK = "link";

    /* Menu parameters */
    String URL_GET_MENU = "http://www.apptosmart.com/service/menu.php";
    String IMG_MENU = "IMGMENU";
    String MEMORY_IMG_MENU = "img_menu";
    String MENU = "MENU";
    /* title object */
    String MENU_TITLE_OBJECT = "title";
    //String MENU_ICON_PARAM = "icon";
    //String MENU_ACTION_PARAM = "action";
    String MENU_LINK_PARAM = "link";
    String MENU_ACTIVE_PARAM = "active";
    int VALUE_TO_BE_SHOWN_IN_ACTIVE_PARAM = 1;
}
