package com.apptosmart.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.apptosmart.AppToSmart;
import com.apptosmart.Constants;
import com.apptosmart.items.ItemMenu;

import java.util.ArrayList;
import java.util.List;

public class TableMenu extends SQLiteDB {

    private static TableMenu mInstance = null;

    public static TableMenu getInstance() {
        if (mInstance == null) {
            mInstance = new TableMenu(AppToSmart.getContext());
        }
        return mInstance;
    }

	public TableMenu(Context context) {
		super(context);
	}

    public void addItem(ItemMenu menu) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Constants.KEY_TITLE, menu.getTitle());
        values.put(Constants.KEY_LINK, menu.getLink());

        db.insert(Constants.TABLE_MENU, null, values);
    }

    @SuppressWarnings("TryFinallyCanBeTryWithResources")
    public List<ItemMenu> getAllMenu() {
        List<ItemMenu> menu = new ArrayList<>();

        String query = "SELECT * FROM " + Constants.TABLE_MENU;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        try {
            ItemMenu item;
            if(cursor.moveToFirst()) {
                do {
                    item = new ItemMenu();
                    item.setId(Long.parseLong(cursor.getString(0)));
                    item.setTitle(cursor.getString(1));
                    item.setLink(cursor.getString(2));
                    menu.add(item);
                } while(cursor.moveToNext());
            }
        } finally {
            cursor.close();
        }
        return menu;
    }
}
