package com.apptosmart.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.apptosmart.Constants;

public class SQLiteDB extends SQLiteOpenHelper {

    public SQLiteDB(Context context) {
        super(context, Constants.DATABASE_NAME, null, Constants.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + Constants.TABLE_MENU + "(" + Constants.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Constants.KEY_TITLE + " TEXT NOT NULL, " + Constants.KEY_LINK + " TEXT NOT NULL)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Constants.TABLE_MENU);
        this.onCreate(db);
    }

    @SuppressWarnings({"unused", "TryFinallyCanBeTryWithResources"})
    public boolean isTableEmpty(String table) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT count(*) FROM " + table, null);
        try {
            cursor.moveToFirst();
            return !(cursor.getInt(0) > 0);
        } finally {
            cursor.close();
        }
    }

    @SuppressWarnings("unused")
    public void deleteAllItems(String table) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + table);
    }

    @SuppressWarnings({"unused", "TryFinallyCanBeTryWithResources"})
    public int getCount(String table) {
        String countQuery = "SELECT * FROM " + table;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        try {
            return cursor.getCount();
        } finally {
            cursor.close();
        }
    }
}
